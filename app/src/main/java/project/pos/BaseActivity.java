package project.pos;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentManager;

import com.android.volley.toolbox.StringRequest;

import java.util.Objects;

import config.Routing;
import preferences.LoginPreferences;
import preferences.UserPreferences;
import request.HttpRequest;

public class BaseActivity extends AppCompatActivity {

    public static int PERMISSION_ACCESS_FINE_LOCATION;
    LoginPreferences loginPrefs;
    UserPreferences userPrefs;
    Routing routing;
    ProgressDialog progressDialog;
    FragmentManager fragmentManager;
    StringRequest request;
    HttpRequest httpRequest;
    Dialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.INTERNET},PERMISSION_ACCESS_FINE_LOCATION);
            return;
        }

        routing     = new Routing();
        loginPrefs  = new LoginPreferences(Objects.requireNonNull(getBaseContext()));
        userPrefs   = new UserPreferences(getBaseContext());
        httpRequest = new HttpRequest(getBaseContext());
        dialog      = new Dialog(this);

        progressDialog  = new ProgressDialog(getBaseContext());
        fragmentManager = getSupportFragmentManager();
    }

    public void hideDialog(){
        if (dialog.isShowing())
            dialog.dismiss();
    }

}
