package project.pos;

import android.os.Bundle;

import config.Utils;
import fragment.LoginFragment;
import fragment.LogoFragment;

public class MainLoginActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_login);

        if (savedInstanceState == null){
            if (getIntent().hasExtra("logout")){
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.frameContainer, new LoginFragment(),
                                Utils.Login_Fragment).commit();
            }else{
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.frameContainer, new LogoFragment(),
                                "Logo_Fragment").commit();
            }
        }
    }
}
