package project.pos;

import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import config.Utils;
import fragment.HomeFragment;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null){
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.frameContainer, new HomeFragment(),
                            Utils.Home_Fragment).commit();
        }
    }

    private Boolean exit = false;
    @Override
    public void onBackPressed() {
        if (exit) {
            finishAffinity();
        } else {
            Toast.makeText(this, "Tekan Back Sekali Lagi untuk Keluar.",
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);
        }
    }
}
