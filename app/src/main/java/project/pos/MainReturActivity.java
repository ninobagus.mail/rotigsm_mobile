package project.pos;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import adapter.ListRetur;
import adapter.ReturAdapter;
import adapter.SpinnerData;
import config.Utils;
import fragment.HomeFragment;
import fragment.HomeReturFragment;
import helper.Alert;

public class MainReturActivity extends BaseActivity implements BottomNavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    BottomNavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_retur);

        if (savedInstanceState == null){
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.frameContainer, new HomeReturFragment(),
                            Utils.Home_Retur_Fragment).commit();
        }
        initView();
        setListener();
    }

    private void initView(){
        navigationView      = findViewById(R.id.nav_bottom);
    }

    private void setListener(){
        navigationView.setOnNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.item1:
                sendBroadcast(new Intent("reloadRetur"));
                break;
            case R.id.item2:
                modalRetur();
                break;
            case R.id.item3:
                loginPrefs.destroyPreferences();
                startActivity(new Intent(getBaseContext(), MainLoginActivity.class).putExtra("logout", true));
                finishAffinity();
                break;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.close:
                if (dialog.isShowing())
                    dialog.dismiss();
                break;
            case R.id.btnRetur:
                validateAddRetur();
                break;
            case R.id.atBahan:
                if (!D_atBahan.isPopupShowing())
                    D_atBahan.showDropDown();
                break;
            case R.id.atSupplier:
                if (!D_atSupplier.isPopupShowing())
                    D_atSupplier.showDropDown();
                break;
            case R.id.etTgl:
                new DatePickerDialog(dialog.getContext(),
                        dateRetur,
                        calendarTanggal.get(Calendar.YEAR),
                        calendarTanggal.get(Calendar.MONTH),
                        calendarTanggal.get(Calendar.DAY_OF_MONTH)
                ).show();
                break;
        }
    }

    private void validateAddRetur(){
        String bahan    = D_atBahan.getText().toString();
        String supplier = D_atSupplier.getText().toString();
        String qty      = D_etQty.getText().toString();
        String tgl      = D_etTgl.getText().toString();
        String price    = D_etPrice.getText().toString();
        String reason   = D_etReason.getText().toString();

        if(bahan.isEmpty() || supplier.isEmpty() || qty.isEmpty()
                || tgl.isEmpty() || price.isEmpty() || reason.isEmpty())
            new Alert().Error(getBaseContext(), navigationView, "Data harus diisi semua !");
        else
            createRetur();
    }

    String id_bahan, id_supplier;
    AutoCompleteTextView D_atBahan, D_atSupplier;
    EditText D_etQty, D_etPrice, D_etTgl, D_etReason;
    SpinnerData dataBahanMentah, dataSupplier;
    ArrayList<Map<String, String>> listBahanMentah, listSupplier;
    Calendar calendarTanggal = Calendar.getInstance();

    DatePickerDialog.OnDateSetListener dateRetur = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int month, int day) {
            calendarTanggal.set(Calendar.YEAR, year);
            calendarTanggal.set(Calendar.MONTH, month);
            calendarTanggal.set(Calendar.DAY_OF_MONTH, day);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            D_etTgl.setText(sdf.format(calendarTanggal.getTime()));
            new TimePickerDialog(dialog.getContext(),
                    timeRetur,
                    calendarTanggal.get(Calendar.HOUR_OF_DAY),
                    calendarTanggal.get(Calendar.MINUTE), true
            ).show();
        }
    };

    TimePickerDialog.OnTimeSetListener timeRetur = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            calendarTanggal.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calendarTanggal.set(Calendar.MINUTE, minute);
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss", Locale.US);
            D_etTgl.append(" "+sdf.format(calendarTanggal.getTime()));
        }
    };

    private void modalRetur() {
        if (dialog.isShowing())
            dialog.dismiss();

        dialog.setContentView(R.layout.modal_retur);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.findViewById(R.id.close).setOnClickListener(this);
        dialog.findViewById(R.id.btnRetur).setOnClickListener(this);

        D_atBahan   = dialog.findViewById(R.id.atBahan);
        D_atSupplier= dialog.findViewById(R.id.atSupplier);
        D_etQty     = dialog.findViewById(R.id.etQty);
        D_etPrice   = dialog.findViewById(R.id.etPriceQty);
        D_etTgl     = dialog.findViewById(R.id.etTgl);
        D_etReason  = dialog.findViewById(R.id.etReason);

        dataBahanMentah = new SpinnerData();
        dataSupplier    = new SpinnerData();

        D_atBahan.setOnClickListener(this);
        D_atSupplier.setOnClickListener(this);
        D_etTgl.setOnClickListener(this);

        D_atBahan.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    D_atBahan.showDropDown();
                else{
                    boolean isBahan = false;
                    for (int i = 0; i < listBahanMentah.size(); i++){
                        if (D_atBahan.getText().toString().equals(dataBahanMentah.getValue(i))){
                            id_bahan = dataBahanMentah.getId(i);
                            isBahan = true;
                        }
                        if (i == listBahanMentah.size()-1 && !isBahan) {
                            D_atBahan.setText("");
                            new Alert().Error(dialog.getContext(), v, "Bahan tidak ada di list");
                        }
                    }
                }
            }
        });

        D_atBahan.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                for (int i = 0; i < listBahanMentah.size(); i++){
                    if (D_atBahan.getText().toString().equals(dataBahanMentah.getValue(i))){
                        id_bahan = dataBahanMentah.getId(i);
                    }
                }
            }
        });

        D_atSupplier.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    D_atSupplier.showDropDown();
                else{
                    boolean isSupplier = false;
                    for (int i = 0; i < listSupplier.size(); i++){
                        if (D_atSupplier.getText().toString().equals(dataSupplier.getValue(i))){
                            id_supplier = dataSupplier.getId(i);
                            isSupplier = true;
                        }
                        if (i == listSupplier.size()-1 && !isSupplier) {
                            D_atSupplier.setText("");
                            new Alert().Error(dialog.getContext(), v, "Supplier tidak ada di list");
                        }
                    }
                }
            }
        });

        D_atSupplier.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                for (int i = 0; i < listSupplier.size(); i++){
                    if (D_atSupplier.getText().toString().equals(dataSupplier.getValue(i))){
                        id_supplier = dataSupplier.getId(i);
                    }
                }
            }
        });

        D_etTgl.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    new DatePickerDialog(dialog.getContext(),
                            dateRetur,
                            calendarTanggal.get(Calendar.YEAR),
                            calendarTanggal.get(Calendar.MONTH),
                            calendarTanggal.get(Calendar.DAY_OF_MONTH)
                    ).show();

            }
        });


        getBahanMentah();
        getSuppliers();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialog.create();
        }
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
    }

    private void getBahanMentah(){
        request = new StringRequest(Request.Method.POST, routing.getBahan, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("BahanMentah", response);
                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    boolean success = jsonResponse.getBoolean("success");
                    if (success){
                        listBahanMentah = new ArrayList<>();
                        JSONArray dataArray = jsonResponse.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject data = dataArray.getJSONObject(i);
                            Map<String, String> bahanMentah = new HashMap<String, String>();
                            bahanMentah.put("id", data.getString("rm_id"));
                            bahanMentah.put("value", data.getString("rm_name"));
                            listBahanMentah.add(bahanMentah);
                        }
                        dataBahanMentah.setData(listBahanMentah);
                        final ArrayAdapter<String> masterAdapter = new ArrayAdapter<String>(getBaseContext(), R.layout.support_simple_spinner_dropdown_item, dataBahanMentah.getListValue());
                        masterAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        D_atBahan.setAdapter(masterAdapter);

                    }else{
                        Toast.makeText(getBaseContext(), jsonResponse.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("outlet_id", loginPrefs.getString("outlet_id"));
                return params;
            }
        };

        httpRequest.addQueue(request);
    }

    private void getSuppliers(){
        request = new StringRequest(Request.Method.POST, routing.getSupplier, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Supplier", response);
                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    boolean success = jsonResponse.getBoolean("success");
                    if (success){
                        listSupplier = new ArrayList<>();
                        JSONArray dataArray = jsonResponse.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject data = dataArray.getJSONObject(i);
                            Map<String, String> supplier = new HashMap<String, String>();
                            supplier.put("id", data.getString("id"));
                            supplier.put("value", data.getString("name"));
                            listSupplier.add(supplier);
                        }
                        dataSupplier.setData(listSupplier);
                        final ArrayAdapter<String> masterAdapter = new ArrayAdapter<String>(getBaseContext(), R.layout.support_simple_spinner_dropdown_item, dataSupplier.getListValue());
                        masterAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        D_atSupplier.setAdapter(masterAdapter);

                    }else{
                        Toast.makeText(getBaseContext(), jsonResponse.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        httpRequest.addQueue(request);
    }

    private void createRetur(){
        request = new StringRequest(Request.Method.POST, routing.addRetur, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("CreateRetur", response);
                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    boolean success = jsonResponse.getBoolean("success");
                    if (success){
                        hideDialog();
                        sendBroadcast(new Intent("reloadRetur"));
                        new Alert().Success(getBaseContext(), navigationView, jsonResponse.getString("message"));
                    }else{
                        new Alert().Error(getBaseContext(), navigationView, jsonResponse.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e("CreateError", Objects.requireNonNull(error.getMessage()));
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", loginPrefs.getString("id"));
                params.put("outlet_id", loginPrefs.getString("outlet_id"));
                params.put("rm_id", id_bahan);
                params.put("supplier_id", id_supplier);
                params.put("qty", D_etQty.getText().toString());
                params.put("price", D_etPrice.getText().toString());
                params.put("trx_date", D_etTgl.getText().toString());
                params.put("reason", D_etReason.getText().toString());
                return params;
            }
        };

        httpRequest.addQueue(request);
    }

//    private Boolean exit = false;
//    @Override
//    public void onBackPressed() {
//        if (exit) {
//            finishAffinity();
//        } else {
//            Toast.makeText(this, "Tekan Back Sekali Lagi untuk Keluar.",
//                    Toast.LENGTH_SHORT).show();
//            exit = true;
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    exit = false;
//                }
//            }, 3 * 1000);
//        }
//    }
}
