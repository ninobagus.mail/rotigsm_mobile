package preferences;

import android.content.Context;
import android.content.SharedPreferences;

public class DevicePreferences {
    private String Tag = "DevicePreferences";
    SharedPreferences preferences;
    SharedPreferences.Editor editor;


    public DevicePreferences(Context context){
        preferences = context.getSharedPreferences(Tag, Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    public void setDevice(String device){
        editor.putString("deviceId", device).commit();
    }

    public String getDevice(){
        return preferences.getString("deviceId", "");
    }
}
