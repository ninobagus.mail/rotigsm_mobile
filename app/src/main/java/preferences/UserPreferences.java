package preferences;

import android.content.Context;
import android.content.SharedPreferences;

public class UserPreferences {
    private String Tag = "UserPreferences";
    SharedPreferences preferences;
    SharedPreferences.Editor editor;


    public UserPreferences(Context context){
        preferences = context.getSharedPreferences(Tag, Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    public void setEmail(String email){
        editor.putString("email", email).commit();
    }

    public String getEmail(){
        return preferences.getString("email", "");
    }
}
