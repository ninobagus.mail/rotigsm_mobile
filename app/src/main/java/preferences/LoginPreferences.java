package preferences;

import android.content.Context;
import android.content.SharedPreferences;

public class LoginPreferences {
    private String Tag = "LoginPreferences";
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    public LoginPreferences(Context context){
        preferences = context.getSharedPreferences(Tag, Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    public void setPrefs(String key_name, String value){
        editor.putString(key_name, value).commit();
    }

    public void setPrefs(String key_name, Integer value){
        editor.putInt(key_name, value).commit();
    }

    public void setPrefs(String key_name, Boolean value){
        editor.putBoolean(key_name, value).commit();
    }

    public void isLogin(Boolean value){
        editor.putBoolean("isLogin", value).commit();
    }

    public Boolean isLogin(){
        return preferences.getBoolean("isLogin", false);
    }

    public String getString(String key_value){
        return preferences.getString(key_value, null);
    }

    public Integer getInt(String key_value){
        return preferences.getInt(key_value, 0);
    }

    public Boolean getBoolean(String key_value){
        return preferences.getBoolean(key_value, false);
    }

    public void destroyPreferences(){
        editor.clear().commit();
    }
}
