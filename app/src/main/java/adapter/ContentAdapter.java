package adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import project.pos.R;

public class ContentAdapter extends RecyclerView.Adapter<ContentAdapter.ViewHolder> {

    private Context context;
    private List<ListContent> adapterList;
    public ContentAdapter(Context context, List<ListContent> adapterList){
        this.context = context; this.adapterList = adapterList;
    }

    @NonNull
    @Override
    public ContentAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.single_content, null);
        return new ContentAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ContentAdapter.ViewHolder holder, int position) {
        ListContent data = adapterList.get(position);
        holder.id = data.getId();
        holder.nama.setText(data.getNama());
        holder.retail_price = data.getRetail_price();
        holder.harga.setText(data.getHarga());
        holder.code.setText(data.getCode());
        Picasso.get().load(data.getUrlImage()).into(holder.contentImg);

    }

    @Override
    public int getItemCount() {
        return adapterList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView nama, harga, code;
        ImageView contentImg;
        String id, retail_price;

        public ViewHolder(final View itemView){
            super(itemView);
            nama    = itemView.findViewById(R.id.txt_nama);
            harga   = itemView.findViewById(R.id.txt_harga);
            code    = itemView.findViewById(R.id.txt_code);
            contentImg  = itemView.findViewById(R.id.contentImg);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((Activity)context).sendBroadcast(new Intent("addProduk")
                            .putExtra("id", id)
                            .putExtra("produk", nama.getText().toString())
                            .putExtra("code", code.getText().toString())
                            .putExtra("harga", retail_price));
                }
            });
        }
    }
}
