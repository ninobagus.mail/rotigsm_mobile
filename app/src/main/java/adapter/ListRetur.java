package adapter;

public class ListRetur {
    private String create_date, no_trx, outlet_name, sup_name, pro_name, qty, price_qty, total_price, reason;

    public ListRetur(String create_date, String no_trx, String outlet_name, String sup_name, String pro_name,
                     String qty, String price_qty, String total_price, String reason){
        this.create_date = create_date; this.no_trx = no_trx; this.sup_name = sup_name;
        this.pro_name = pro_name; this.qty = qty; this.price_qty = price_qty;
        this.total_price = total_price; this.reason = reason ; this.outlet_name = outlet_name;

    }

    public String getCreate_date() {
        return create_date;
    }

    public String getNo_trx() {
        return no_trx;
    }

    public String getSup_name() {
        return sup_name;
    }

    public String getPro_name() {
        return pro_name;
    }

    public String getQty() {
        return qty;
    }

    public String getPrice_qty() {
        return price_qty;
    }

    public String getTotal_price() {
        return total_price;
    }

    public String getReason() {
        return reason;
    }

    public String getOutlet_name() {
        return outlet_name;
    }
}
