package adapter;

public class ListProduk {
    private String id_produk, nama, code, harga, qty;

    public ListProduk(String id_produk, String nama, String code, String harga, String qty){
        this.id_produk = id_produk; this.nama = nama; this.code = code; this.harga = harga; this.qty = qty;
    }

    public String getId_produk() {
        return id_produk;
    }

    public String getNama() {
        return nama;
    }

    public String getCode() {
        return code;
    }

    public String getQty() {
        return qty;
    }

    public String getHarga() {
        return harga;
    }
}
