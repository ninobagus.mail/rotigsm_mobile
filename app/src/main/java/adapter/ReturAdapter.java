package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import project.pos.R;

public class ReturAdapter extends RecyclerView.Adapter<ReturAdapter.ViewHolder> {

    private Context context;
    private List<ListRetur> adapterList;
    public ReturAdapter(Context context, List<ListRetur> adapterList){
        this.context = context; this.adapterList = adapterList;
    }

    @NonNull
    @Override
    public ReturAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.single_retur, null);
        return new ReturAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ReturAdapter.ViewHolder holder, int position) {
        ListRetur data = adapterList.get(position);
        holder.date.setText(data.getCreate_date());
        holder.trx_no.setText(data.getNo_trx());
        holder.outlet_name.setText(data.getOutlet_name());
        holder.sup_name.setText(data.getSup_name());
        holder.pro_name.setText(data.getPro_name());
        holder.qty.setText(data.getQty());
        holder.price_qty.setText(data.getPrice_qty());
        holder.ttl_price.setText(data.getTotal_price());
        holder.reason.setText(data.getReason());
    }

    @Override
    public int getItemCount() {
        return adapterList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView date, trx_no, outlet_name, sup_name, pro_name, qty, price_qty, ttl_price, reason;

        public ViewHolder(final View itemView){
            super(itemView);
            date        = itemView.findViewById(R.id.create_date);
            trx_no      = itemView.findViewById(R.id.no_trx);
            outlet_name = itemView.findViewById(R.id.outlet_name);
            sup_name    = itemView.findViewById(R.id.supplier_name);
            pro_name    = itemView.findViewById(R.id.product_name);
            qty         = itemView.findViewById(R.id.qty);
            price_qty   = itemView.findViewById(R.id.price);
            ttl_price   = itemView.findViewById(R.id.ttlPrice);
            reason      = itemView.findViewById(R.id.reason);

        }

    }
}
