package adapter;

public class ListContent {
    private String id, nama, code, retail_price, harga, urlImage;

    public ListContent(String id, String nama, String code, String retail_price, String harga, String urlImage){
        this.id = id; this.nama = nama; this.code = code;
        this.retail_price = retail_price; this.harga = harga; this.urlImage = urlImage;
    }

    public String getId() {
        return id;
    }

    public String getNama() {
        return nama;
    }

    public String getCode() {
        return code;
    }

    public String getRetail_price() {
        return retail_price;
    }

    public String getHarga() {
        return harga;
    }

    public String getUrlImage() {
        return urlImage;
    }
}
