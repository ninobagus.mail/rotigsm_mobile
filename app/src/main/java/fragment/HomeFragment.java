package fragment;

import android.app.Activity;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import adapter.ListProduk;
import adapter.ProdukAdapter;
import bluetooth_utils.DeviceList;
import bluetooth_utils.PrinterCommands;
import helper.Alert;
import config.Utils;
import project.pos.MainLoginActivity;
import project.pos.MainReturActivity;
import project.pos.R;

public class HomeFragment extends BaseFragment implements View.OnClickListener, BottomNavigationView.OnNavigationItemSelectedListener {

    View view;
    LinearLayout layoutBottomSheet;
    BottomSheetBehavior behavior;
    BottomNavigationView navigationView;
    Boolean menu = false;

    private RecyclerView recyclerView;
    private ProdukAdapter produkAdapter;
    private List<ListProduk> listProduks;

    String message = "order";
    private TextView tvTb, tvTtl, tvPjk, tvSubTtl;
    private EditText etDisc;
    EditText et_bayar, et_kembali;
    DecimalFormat dec = new DecimalFormat("#");
    private boolean isPersen = false;
    BroadcastReceiver broadcastReceiver;

//    Modul Bluetooth
    byte FONT_TYPE;
    private static BluetoothSocket btsocket;
    private static OutputStream outputStream;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        initView();
        setListener();
        return view;
    }

    private void initView(){

        navigationView      = view.findViewById(R.id.nav_bottom);
        layoutBottomSheet   = view.findViewById(R.id.bottom_sheet);
        behavior = BottomSheetBehavior.from(layoutBottomSheet);

        tvTb    = view.findViewById(R.id.tv_tb);
        tvTtl   = view.findViewById(R.id.tv_ttl);
        tvPjk   = view.findViewById(R.id.tv_pjk);
        tvSubTtl= view.findViewById(R.id.tv_subTtl);
        etDisc  = view.findViewById(R.id.et_disc);

        recyclerView = view.findViewById(R.id.recycleProduk);
        recyclerView.setHasFixedSize(true);
        listProduks = new ArrayList<>();
    }

    private void setListener(){
        navigationView.setOnNavigationItemSelectedListener(this);

//        tvUser.setText(" Nama \t\t\t : "+loginPrefs.getString("fullname")
//                +" \n Email \t\t\t : "+loginPrefs.getString("email")
//                +" \n Role Id \t\t : "+loginPrefs.getString("role_id")
//                +" \n Outlet Id \t : "+loginPrefs.getString("outlet_id"));

        behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
                if(newState == BottomSheetBehavior.STATE_EXPANDED){
                    if (!menu) {
                        menu = true;
                        fragmentManager
                                .beginTransaction()
                                .replace(R.id.frameSheet, new HomeAllFragment(),
                                        Utils.Home_Content_Fragment).commit();
                    }
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        etDisc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                float disc;
                float total = Integer.parseInt(tvTtl.getText().toString());
                if (s.toString().isEmpty())
                    disc = total - 0;
                else{
                    if (isPersen){
                        if (Float.parseFloat(s.toString()) > 100){
                            new Alert().Error(getContext(), view, "Diskon melebihi 100%");
                            etDisc.setText("100");
                            disc = 100/100*total;
                        }else if(Float.parseFloat(s.toString()) < 0){
                            new Alert().Error(getContext(), view, "Diskon kurang dari 0%");
                            etDisc.setText("");
                            disc = total - 0;
                        }else{
                            disc = Float.parseFloat(s.toString())/100*total;
                        }
                        disc = total - disc;
                    }else{
                        if (Float.parseFloat(s.toString()) > total) {
                            new Alert().Error(getContext(), view, "Diskon melebihi total harga pemebelian");
                            etDisc.setText(String.valueOf(dec.format(total)));
                            disc = total - total;
                        }else if(Float.parseFloat(s.toString()) < 0){
                            new Alert().Error(getContext(), view, "Diskon kurang dari 0");
                            disc = total - 0;
                        }else{
                            disc = total - Float.parseFloat(s.toString());
                        }
                    }
                }
                tvSubTtl.setText(String.valueOf(dec.format(disc)));
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

        view.findViewById(R.id.tvPersen).setOnClickListener(this);
        view.findViewById(R.id.btnBatal).setOnClickListener(this);
        view.findViewById(R.id.btnBayar).setOnClickListener(this);

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                switch (action) {
                    case "addProduk":
                        addProduk(intent.getStringExtra("id")
                                , intent.getStringExtra("produk")
                                , intent.getStringExtra("code")
                                , intent.getStringExtra("harga"), "1");
                        break;
                    case "plusProduk":
                        plusminProduk(intent.getStringExtra("id"), true);
                        break;
                    case "minProduk":
                        plusminProduk(intent.getStringExtra("id"), false);
                        break;
                    case "removeProduk":
                        removeProduk(intent.getStringExtra("id"));
                        break;
                }
            }
        };

        IntentFilter intentFilter = new IntentFilter("addProduk");
        intentFilter.addAction("plusProduk");
        intentFilter.addAction("minProduk");
        intentFilter.addAction("removeProduk");
        getContext().registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tvPersen:
                float disc;
                float total = Integer.parseInt(tvTtl.getText().toString());
                if (!isPersen) {
                    view.findViewById(R.id.tvPer).setVisibility(View.VISIBLE);
                    isPersen = true;
                    if (etDisc.getText().toString().isEmpty())
                        disc = total - 0;
                    else {
                        if (Float.parseFloat(etDisc.getText().toString()) > 100) {
                            etDisc.setText("100");
                            disc = 100 / 100 * total;
                        } else if (Float.parseFloat(etDisc.getText().toString()) < 0) {
                            etDisc.setText("");
                            disc = total - 0;
                        } else {
                            disc = (Float.parseFloat(etDisc.getText().toString()) / 100)* total;
                        }
                        disc = total - disc;
                    }
                    tvSubTtl.setText(String.valueOf(dec.format(disc)));
                }else{
                    view.findViewById(R.id.tvPer).setVisibility(View.GONE);
                    isPersen = false;
                    if (etDisc.getText().toString().isEmpty())
                        disc = total - 0;
                    else {
                        if (Float.parseFloat(etDisc.getText().toString()) > total) {
                            new Alert().Error(getContext(), view, "Diskon melebihi total harga pemebelian");
                            etDisc.setText(String.valueOf(dec.format(total)));
                            disc = total - total;
                        }else if(Float.parseFloat(etDisc.getText().toString()) < 0){
                            new Alert().Error(getContext(), view, "Diskon kurang dari 0");
                            disc = total - 0;
                        }else{
                            disc = total - Float.parseFloat(etDisc.getText().toString());
                        }
                    }
                    tvSubTtl.setText(String.valueOf(dec.format(disc)));
                }
                break;
            case R.id.btnBatal:
                listProduks.clear();
                showProduk();
                break;
            case R.id.btnBayar:
                if (listProduks.size() > 0)

                    modalBayar();
                else
                    new Alert().Error(getContext(), v, "Tidak ada pembelian yang dapat di proses");
                break;
            case R.id.btnPebayaran:
                Float bayar = (et_bayar.getText().toString().isEmpty())?0:Float.parseFloat(et_bayar.getText().toString());
                if (bayar > 0 && Float.parseFloat(et_kembali.getText().toString()) <= bayar)
                    bayarProduk();
                else
                    new Alert().Error(getContext(), v, "Pembayaran kurang dari total yang harus di bayar");
                break;
            case R.id.close:
                if (dialog.isShowing())
                    dialog.dismiss();
                break;
            case R.id.btnMenuRetur:
                hideDialog();
                startActivity(new Intent(getContext(), MainReturActivity.class));
                break;
            case R.id.btnMenuLogout:
                loginPrefs.destroyPreferences();
                startActivity(new Intent(getContext(), MainLoginActivity.class).putExtra("logout", true));
                ((Activity)getActivity()).finishAffinity();
                break;
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment;
        Bundle arg = new Bundle();
        switch (item.getItemId()){
            case R.id.item1:
                menu = true;
                item.setChecked(true);
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.frameSheet, new HomeAllFragment(),
                                Utils.Home_Content_Fragment).commit();
                if (behavior.getState() == BottomSheetBehavior.STATE_COLLAPSED)
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                break;
            case R.id.item2:
                menu = true;
                item.setChecked(true);
                fragment = new HomeAllFragment();
                arg.putString("category", "10");
                fragment.setArguments(arg);
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.frameSheet, fragment,
                                Utils.Home_Content_Fragment).commit();
                if (behavior.getState() == BottomSheetBehavior.STATE_COLLAPSED)
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                break;
            case R.id.item3:
                menu = true;
                item.setChecked(true);
                fragment = new HomeAllFragment();
                arg.putString("category", "11");
                fragment.setArguments(arg);
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.frameSheet, fragment,
                                Utils.Home_Content_Fragment).commit();
                if (behavior.getState() == BottomSheetBehavior.STATE_COLLAPSED)
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                break;
            case R.id.item4:
                menu = true;
                item.setChecked(true);
                fragment = new HomeAllFragment();
                arg.putString("category", "12");
                fragment.setArguments(arg);
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.frameSheet, fragment,
                                Utils.Home_Content_Fragment).commit();
                if (behavior.getState() == BottomSheetBehavior.STATE_COLLAPSED)
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                break;
            case R.id.item5:
                if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED)
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                else{
                    modalMenu();
                }
                break;
        }
        return false;
    }

    private void showProduk(){
        produkAdapter = new ProdukAdapter(getContext(), listProduks);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(produkAdapter);

        int S_tb = 0, S_ttl = 0;
        for (int i = 0; i < listProduks.size(); i++){
            S_tb  += Integer.parseInt(listProduks.get(i).getQty());
            S_ttl += Float.parseFloat(listProduks.get(i).getQty())*Float.parseFloat(listProduks.get(i).getHarga());
        }

        tvTb.setText(String.valueOf(S_tb));
        tvTtl.setText(String.valueOf(dec.format(S_ttl)));
        tvSubTtl.setText(String.valueOf(dec.format(S_ttl)));
    }

    private void addProduk(String id, String nama, String code, String harga, String qty){
        harga = String.valueOf(dec.format(Float.parseFloat(harga)));
        boolean isProduk = false;
        for (int i = 0; i < listProduks.size(); i++){
            if (!isProduk && listProduks.get(i).getId_produk().equals(id)){
                int qtyPlus = Integer.parseInt(listProduks.get(i).getQty())+1;
                checkUpdateQty(i, id, listProduks.get(i).getNama()
                        , listProduks.get(i).getCode(), listProduks.get(i).getHarga(), qtyPlus);
                isProduk = true;
            }
        }
        if(!isProduk){
            checkAddQty(id, nama, code, harga, Integer.parseInt(qty));
        }
    }

    private void plusminProduk(String id, boolean plus){
        for (int i = 0; i < listProduks.size(); i++){
            if (listProduks.get(i).getId_produk().equals(id)){
                int qty;
                if (plus)
                    qty = Integer.parseInt(listProduks.get(i).getQty())+1;
                else {
                    qty = Integer.parseInt(listProduks.get(i).getQty()) - 1;
                }
                if (qty >= 1)
                    checkUpdateQty(i, id, listProduks.get(i).getNama()
                            , listProduks.get(i).getCode(), listProduks.get(i).getHarga(), qty);
                else
                    new Alert().Error(getContext(), view, "Quantity harus lebih dari 0");
            }
        }
    }

    private void removeProduk(String id){
        for (int i = 0; i < listProduks.size(); i++){
            if (listProduks.get(i).getId_produk().equals(id)){
                listProduks.remove(i);
            }
        }
        showProduk();
    }

    private void checkAddQty(final String id, final String nama, final String code, final String harga, final int qty){
        request = new StringRequest(Request.Method.POST, routing.getQty, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Qty", response);
                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    boolean success = jsonResponse.getBoolean("success");
                    if (success){
                        JSONObject data = jsonResponse.getJSONObject("data");
                        if (qty <= data.getInt("qty")) {
                            listProduks.add(new ListProduk(id, nama, code, harga, String.valueOf(qty)));
                            showProduk();
                        }else
                            new Alert().Error(getContext(), view, "Stok Habis !");
                    }else{
                        new Alert().Error(getContext(), view, jsonResponse.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("produk_id", id);
                params.put("outlet_id", loginPrefs.getString("outlet_id"));
                return params;
            }
        };
        httpRequest.addQueue(request);
    }

    private void checkUpdateQty(final int set, final String id, final String nama, final String code, final String harga, final int qty){
        request = new StringRequest(Request.Method.POST, routing.getQty, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Qty", response);
                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    boolean success = jsonResponse.getBoolean("success");
                    if (success){
                        JSONObject data = jsonResponse.getJSONObject("data");
                        if (qty <= data.getInt("qty")) {
                            listProduks.set(set, new ListProduk(id, nama, code, harga, String.valueOf(qty)));
                            showProduk();
                        }else
                            new Alert().Error(getContext(), view, "Stok Habis !");
                    }else{
                        new Alert().Error(getContext(), view, jsonResponse.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("produk_id", id);
                params.put("outlet_id", loginPrefs.getString("outlet_id"));
                return params;
            }
        };

        httpRequest.addQueue(request);
    }

    private void bayarProduk(){
        if(btsocket == null){
            Intent BTIntent = new Intent(Objects.requireNonNull(getActivity()).getApplicationContext(), DeviceList.class);
            this.startActivityForResult(BTIntent, DeviceList.REQUEST_CONNECT_BT);
        }else{
            OutputStream opstream = null;
            try {
                opstream = btsocket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
            outputStream = opstream;

            request = new StringRequest(Request.Method.POST, routing.bayar, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("Bayar", response);
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        boolean success = jsonResponse.getBoolean("success");
                        if (success){
                            JSONObject data = jsonResponse.getJSONObject("data");
                            new Alert().Success(getContext(), view, jsonResponse.getString("message"));
                            if (dialog.isShowing())
                                dialog.dismiss();
                            CetakBill();
                            listProduks.clear();
                            showProduk();
                            Bundle args = new Bundle();
                            args.putString("url", data.getString("url"));
                            args.putString("urlpdf", data.getString("urlPdf"));
                            Fragment fragment = new InvoiceFragment();
                            fragment.setArguments(args);
                            fragmentManager
                                    .beginTransaction().setCustomAnimations(R.anim.right_enter, R.anim.left_out)
                                    .replace(R.id.frameContainer, fragment,
                                            Utils.Invoice_Fragment).commit();

                            //modul mulai print


                        }else{
                            new Alert().Error(getContext(), view, jsonResponse.getString("message"));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    for (int i = 0; i < listProduks.size(); i++) {
                        params.put("dataProduk["+i+"][id]", listProduks.get(i).getId_produk());
                        params.put("dataProduk["+i+"][qty]", listProduks.get(i).getQty());
                    }
                    params.put("isProduk", (listProduks.size()>0)?"true":"false");
                    if (listProduks.size() > 0) {
                        params.put("paid_amt", et_bayar.getText().toString());
                        params.put("return_change", et_kembali.getText().toString());
                        params.put("payment_method", "1");
                        params.put("total_items", tvTb.getText().toString());
                        params.put("grandtotal", tvSubTtl.getText().toString());
                        params.put("tax", "0");
                        params.put("discount_percentage", (isPersen) ? etDisc.getText().toString() : "");
                        params.put("discount_total", (!isPersen) ? etDisc.getText().toString() : "0");
                        params.put("subtotal", tvTtl.getText().toString());
                        params.put("outlet_id", loginPrefs.getString("outlet_id"));
                        params.put("ordered_datetime", localization.stringDateTime(localization.getTime()));
                        params.put("customer_id", "1");
                        params.put("created_user_id", loginPrefs.getString("id"));
                    }
                    return params;
                }
            };

            httpRequest.addQueue(request);
        }
    }

    private void modalMenu() {
        if (dialog.isShowing())
            dialog.dismiss();

        dialog.setContentView(R.layout.modal_menu);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.findViewById(R.id.close).setOnClickListener(this);
        dialog.findViewById(R.id.btnMenuRetur).setOnClickListener(this);
        dialog.findViewById(R.id.btnMenuLogout).setOnClickListener(this);

        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialog.create();
        }
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
    }

    private void modalBayar(){
        if (dialog.isShowing())
            dialog.dismiss();

        dialog.setContentView(R.layout.modal_pebayaran);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.findViewById(R.id.close).setOnClickListener(this);
        dialog.findViewById(R.id.btnPebayaran).setOnClickListener(this);
        EditText et_ttlBayar    = dialog.findViewById(R.id.total_bayar);
        EditText et_ttlBarang   = dialog.findViewById(R.id.total_barang);
        et_bayar       = dialog.findViewById(R.id.jumlah_bayar);
        et_kembali     = dialog.findViewById(R.id.kembalian);
        et_ttlBayar.setText(tvTtl.getText());
        et_ttlBarang.setText(tvTb.getText());

        et_bayar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int total = Integer.parseInt(tvTtl.getText().toString());
                if (s.toString().isEmpty())
                    et_kembali.setText("0");
                else{
                    int kembalian = Integer.parseInt(s.toString())- total;
                    et_kembali.setText(String.valueOf(kembalian));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialog.create();
        }
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
    }
    public void printUnicode(){
        try {
            outputStream.write(PrinterCommands.ESC_ALIGN_CENTER);
            printText(bluetooth_utils.Utils.UNICODE_TEXT);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String leftRightAlign(String str1, String str2) {
        String ans = str1 +str2;
        if(ans.length() <31){
            int n = (31 - str1.length() + str2.length());
            ans = str1 + new String(new char[n]).replace("\0", " ") + str2;
        }
        return ans;
    }

    private void printNewLine() {
        try {
            outputStream.write(PrinterCommands.FEED_LINE);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void resetPrint() {
        try{
            outputStream.write(PrinterCommands.ESC_FONT_COLOR_DEFAULT);
            outputStream.write(PrinterCommands.FS_FONT_ALIGN);
            outputStream.write(PrinterCommands.ESC_ALIGN_LEFT);
            outputStream.write(PrinterCommands.ESC_CANCEL_BOLD);
            outputStream.write(PrinterCommands.LF);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void CetakBill() {
        if(btsocket == null){
            Intent BTIntent = new Intent(getActivity().getApplicationContext(), DeviceList.class);
            this.startActivityForResult(BTIntent, DeviceList.REQUEST_CONNECT_BT);
        }
        else{
            OutputStream opstream = null;
            try {
                opstream = btsocket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
            outputStream = opstream;

            //print command
            try {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                outputStream = btsocket.getOutputStream();
                byte[] printformat = new byte[]{0x1B,0x21,0x03};
                outputStream.write(printformat);

                String dateTime[] = getDateTime();
                printCustom(new String(new char[24]).replace("\0", "*"),0,1);
                printNewLine();
                printPhoto(R.drawable.gsmlogo);
                printCustom(new String(new char[24]).replace("\0", "_"),0,1);
                printCustom("Address, Bekasi Jabar",0,0);
                printCustom("Telephone: 089529303412",0,0);
                printCustom("Date : "+dateTime[0]+" "+dateTime[1],0,0);
                printCustom(new String(new char[24]).replace("\0", "_"),0,1);
                printCustom("#  "+ "Product" +"  Qty"+"  Total",0,0);
                printCustom(new String(new char[24]).replace("\0", "_"),0,1);
                int no = 0;
                no++;
                 for (int i = 0; i < listProduks.size(); i++) {
                     printCustom(no  +"   "+listProduks.get(i).getNama() + "   "+ listProduks.get(i).getQty() + "   "+ listProduks.get(i).getHarga(),0,0);
                 }
                printCustom(new String(new char[24]).replace("\0", "_"),0,1);
                printCustom("Grand Total  "+"     "+ tvSubTtl.getText().toString(),0,0);
                printCustom("Paid Amount  "+"     "+ et_bayar.getText().toString(),0,0);
                printCustom("Return Change   "+"   "+ et_kembali.getText().toString(),0,0);
                printCustom(new String(new char[24]).replace("\0", "_"),0,1);
                printNewLine();
                printCustom("Terima Kasih",0,1);
                printCustom(new String(new char[24]).replace("\0", "*"),0,1);
                printNewLine();
                printNewLine();

                outputStream.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    public void printPhoto(int img) {
        try {
            Bitmap bmp = BitmapFactory.decodeResource(getResources(),
                    img);
            if(bmp!=null){
                byte[] command = bluetooth_utils.Utils.decodeBitmap(bmp);
                outputStream.write(PrinterCommands.ESC_ALIGN_CENTER);
                printText(command);
            }else{
                Log.e("Print Photo error", "the file isn't exists");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("PrintTools", "the file isn't exists");
        }
    }

    private void printCustom(String msg, int size, int align) {
        //Print config "mode"
        byte[] cc = new byte[]{0x1B,0x21,0x00};  // 0- normal size text
        //byte[] cc1 = new byte[]{0x1B,0x21,0x00};  // 0- normal size text
        byte[] bb = new byte[]{0x1B,0x21,0x08};  // 1- only bold text
        byte[] bb2 = new byte[]{0x1B,0x21,0x20}; // 2- bold with medium text
        byte[] bb3 = new byte[]{0x1B,0x21,0x10}; // 3- bold with large text
        try {
            switch (size){
                case 0:
                    outputStream.write(cc);
                    break;
                case 1:
                    outputStream.write(bb);
                    break;
                case 2:
                    outputStream.write(bb2);
                    break;
                case 3:
                    outputStream.write(bb3);
                    break;
            }

            switch (align){
                case 0:
                    //left align
                    outputStream.write(PrinterCommands.ESC_ALIGN_LEFT);
                    break;
                case 1:
                    //center align
                    outputStream.write(PrinterCommands.ESC_ALIGN_CENTER);
                    break;
                case 2:
                    //right align
                    outputStream.write(PrinterCommands.ESC_ALIGN_RIGHT);
                    break;
            }
            outputStream.write(msg.getBytes());
            outputStream.write(PrinterCommands.LF);
            //outputStream.write(cc);
            //printNewLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String[] getDateTime() {
        final Calendar c = Calendar.getInstance();
        String dateTime [] = new String[2];
        dateTime[0] = c.get(Calendar.DAY_OF_MONTH) +"/"+ c.get(Calendar.MONTH) +"/"+ c.get(Calendar.YEAR);
        dateTime[1] = c.get(Calendar.HOUR_OF_DAY) +":"+ c.get(Calendar.MINUTE);
        return dateTime;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            btsocket = DeviceList.getSocket();
            if(btsocket != null){
                printText(message);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void printText(String msg) {
        try {
            // Print normal text
            outputStream.write(msg.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void printText(byte[] msg) {
        try {
            // Print normal text
            outputStream.write(msg);
            printNewLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
