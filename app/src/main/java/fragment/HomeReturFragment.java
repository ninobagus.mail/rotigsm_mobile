package fragment;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import adapter.ContentAdapter;
import adapter.ListContent;
import adapter.ListRetur;
import adapter.ReturAdapter;
import adapter.SpinnerData;
import helper.Alert;
import project.pos.R;

public class HomeReturFragment extends BaseFragment implements View.OnClickListener {

    View view;
    private RecyclerView recyclerView;
    private ReturAdapter returAdapter;
    private List<ListRetur> listReturs;
    String id_bahan;
    AutoCompleteTextView atBahan;
    EditText tglAwal, tglAkhir, kodeRetur;
    SpinnerData dataBahanMentah;
    ArrayList<Map<String, String>> listBahanMentah;
    Calendar calendarTanggal = Calendar.getInstance();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home_retur, container, false);
        initView();
        setListener();
        getBahanMentah();
        getRetur();
        return view;
    }

    private void initView(){
        dataBahanMentah = new SpinnerData();
        recyclerView = view.findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);

        atBahan     = view.findViewById(R.id.atBahan);
        tglAwal     = view.findViewById(R.id.etTglAwal);
        tglAkhir    = view.findViewById(R.id.etTglAkhir);
        kodeRetur   = view.findViewById(R.id.etKode);
    }

    private void setListener(){
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);
        recyclerView.setItemAnimator(itemAnimator);
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (intent.getAction()){
                    case "reloadRetur":
                        getRetur();
                        break;
                }
            }
        };

        IntentFilter filter = new IntentFilter("reloadRetur");
        getContext().registerReceiver(broadcastReceiver, filter);

        atBahan.setOnClickListener(this);
        tglAwal.setOnClickListener(this);
        tglAkhir.setOnClickListener(this);
        view.findViewById(R.id.btnSearch).setOnClickListener(this);
        view.findViewById(R.id.btnReset).setOnClickListener(this);

        atBahan.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    atBahan.showDropDown();
                else{
                    boolean isBahan = false;
                    for (int i = 0; i < listBahanMentah.size(); i++){
                        if (atBahan.getText().toString().equals(dataBahanMentah.getValue(i))){
                            id_bahan = dataBahanMentah.getId(i);
                            isBahan = true;
                        }
                        if (i == listBahanMentah.size()-1 && !isBahan) {
                            atBahan.setText("");
                            new Alert().Error(dialog.getContext(), v, "Bahan tidak ada di list");
                        }
                    }
                }
            }
        });

        atBahan.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                for (int i = 0; i < listBahanMentah.size(); i++){
                    if (atBahan.getText().toString().equals(dataBahanMentah.getValue(i))){
                        id_bahan = dataBahanMentah.getId(i);
                    }
                }
            }
        });

        tglAwal.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    new DatePickerDialog(dialog.getContext(),
                            dateAwal,
                            calendarTanggal.get(Calendar.YEAR),
                            calendarTanggal.get(Calendar.MONTH),
                            calendarTanggal.get(Calendar.DAY_OF_MONTH)
                    ).show();

            }
        });

        tglAkhir.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    new DatePickerDialog(dialog.getContext(),
                            dateAhir,
                            calendarTanggal.get(Calendar.YEAR),
                            calendarTanggal.get(Calendar.MONTH),
                            calendarTanggal.get(Calendar.DAY_OF_MONTH)
                    ).show();

            }
        });
    }

    @Override
    public void onClick(View v) {
        InputMethodManager inputManager;
        switch (v.getId()){
            case R.id.btnSearch:
                inputManager = (InputMethodManager)getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                getRetur();
                break;
            case R.id.btnReset:
                inputManager = (InputMethodManager)getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                kodeRetur.setText("");
                atBahan.setText("");
                tglAwal.setText("");
                tglAkhir.setText("");
                kodeRetur.clearFocus();
                break;
            case R.id.atBahan:
                if (!atBahan.isPopupShowing())
                    atBahan.showDropDown();
                break;
            case R.id.etTglAwal:
                new DatePickerDialog(dialog.getContext(),
                        dateAwal,
                        calendarTanggal.get(Calendar.YEAR),
                        calendarTanggal.get(Calendar.MONTH),
                        calendarTanggal.get(Calendar.DAY_OF_MONTH)
                ).show();
                break;
            case R.id.etTglAkhir:
                new DatePickerDialog(dialog.getContext(),
                        dateAhir,
                        calendarTanggal.get(Calendar.YEAR),
                        calendarTanggal.get(Calendar.MONTH),
                        calendarTanggal.get(Calendar.DAY_OF_MONTH)
                ).show();
                break;
        }
    }

    DatePickerDialog.OnDateSetListener dateAwal = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int month, int day) {
            calendarTanggal.set(Calendar.YEAR, year);
            calendarTanggal.set(Calendar.MONTH, month);
            calendarTanggal.set(Calendar.DAY_OF_MONTH, day);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            tglAwal.setText(sdf.format(calendarTanggal.getTime()));
        }
    };

    DatePickerDialog.OnDateSetListener dateAhir = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int month, int day) {
            calendarTanggal.set(Calendar.YEAR, year);
            calendarTanggal.set(Calendar.MONTH, month);
            calendarTanggal.set(Calendar.DAY_OF_MONTH, day);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            tglAkhir.setText(sdf.format(calendarTanggal.getTime()));
        }
    };

    private void getBahanMentah(){
        request = new StringRequest(Request.Method.POST, routing.getBahan, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("BahanMentah", response);
                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    boolean success = jsonResponse.getBoolean("success");
                    if (success){
                        listBahanMentah = new ArrayList<>();
                        JSONArray dataArray = jsonResponse.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject data = dataArray.getJSONObject(i);
                            Map<String, String> bahanMentah = new HashMap<String, String>();
                            bahanMentah.put("id", data.getString("rm_id"));
                            bahanMentah.put("value", data.getString("rm_name"));
                            listBahanMentah.add(bahanMentah);
                        }
                        dataBahanMentah.setData(listBahanMentah);
                        final ArrayAdapter<String> masterAdapter = new ArrayAdapter<String>(getContext(), R.layout.support_simple_spinner_dropdown_item, dataBahanMentah.getListValue());
                        masterAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        atBahan.setAdapter(masterAdapter);

                    }else{
                        new Alert().Error(getContext(), view, jsonResponse.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("outlet_id", loginPrefs.getString("outlet_id"));
                return params;
            }
        };

        httpRequest.addQueue(request);
    }

    public void getRetur(){
        view.findViewById(R.id.txtWait).setVisibility(View.VISIBLE);
        view.findViewById(R.id.scrollview).setVisibility(View.GONE);
        request = new StringRequest(Request.Method.POST, routing.getRetur, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Retur", response);
                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    boolean success = jsonResponse.getBoolean("success");
                    if (success){
                        listReturs = new ArrayList<>();
                        view.findViewById(R.id.txtWait).setVisibility(View.GONE);
                        view.findViewById(R.id.scrollview).setVisibility(View.VISIBLE);
                        String date, no_trx, outlet_name ,sup_name, pro_name, qty, price_qty, ttl_price, reason;
                        JSONArray dataArray = jsonResponse.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject data = dataArray.getJSONObject(i);
                            date        = data.getString("create_date");
                            no_trx      = data.getString("trx_no");
                            outlet_name = data.getString("outlet_name");
                            sup_name    = data.getString("sup_name");
                            pro_name    = data.getString("pro_name");
                            qty         = data.getString("qty");
                            price_qty   = data.getString("price_per_qty");
                            ttl_price   = data.getString("total_price");
                            reason      = data.getString("reason");

                            listReturs.add(new ListRetur(date, no_trx, outlet_name, sup_name ,
                                    pro_name, qty, price_qty, ttl_price, reason));
                        }
                        returAdapter = new ReturAdapter(getContext(), listReturs);
                        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                        recyclerView.setAdapter(returAdapter);

                    }else{
                        new Alert().Error(getContext(), view, jsonResponse.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("outlet_id", loginPrefs.getString("outlet_id"));
                if (!kodeRetur.getText().toString().isEmpty())
                    params.put("no_trx", kodeRetur.getText().toString());
                if (!atBahan.getText().toString().isEmpty())
                    params.put("rm_id", id_bahan);
                if (!tglAwal.getText().toString().isEmpty())
                    params.put("tglAwal", tglAwal.getText().toString());
                if (!tglAkhir.getText().toString().isEmpty())
                    params.put("tglAkhir", tglAkhir.getText().toString());
                return params;
            }
        };

        httpRequest.addQueue(request);
    }

}
