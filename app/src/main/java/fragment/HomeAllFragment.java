package fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import adapter.ContentAdapter;
import adapter.ListContent;
import helper.Alert;
import project.pos.R;

public class HomeAllFragment extends BaseFragment {

    View view;
    private RecyclerView recyclerView;
    private ContentAdapter contentAdapter;
    private List<ListContent> listContent;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home_content, container, false);
        initView();
        setListener();
        return view;
    }

    private void initView(){
        recyclerView = view.findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        listContent = new ArrayList<>();
    }

    private void setListener(){
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);
        recyclerView.setItemAnimator(itemAnimator);
        getProduct();
    }

    public void getProduct(){
        request = new StringRequest(Request.Method.POST, routing.getProducts, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Products", response);
                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    boolean success = jsonResponse.getBoolean("success");
                    if (success){
                        view.findViewById(R.id.txtWait).setVisibility(View.GONE);
                        String T_id, T_name, T_retail_price, T_price, T_code, T_urlImage;
                        JSONArray dataArray = jsonResponse.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject data = dataArray.getJSONObject(i);
                            T_id        = data.getString("id");
                            T_name      = data.getString("name");
                            T_retail_price     = data.getString("retail_price");
                            T_price     = data.getString("price");
                            T_code      = data.getString("code");
                            T_urlImage  = data.getString("urlImage");
                            listContent.add(new ListContent(T_id, T_name, T_code, T_retail_price ,T_price, T_urlImage));
                        }
                        contentAdapter = new ContentAdapter(getContext(), listContent);
                        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                        recyclerView.setAdapter(contentAdapter);

                    }else{
                        new Alert().Error(getContext(), view, jsonResponse.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                if (getArguments() != null && getArguments().containsKey("category"))
                    params.put("category", getArguments().getString("category"));
                return params;
            }
        };

        httpRequest.addQueue(request);
    }
}
