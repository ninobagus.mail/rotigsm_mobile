package fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import config.Utils;
import project.pos.MainActivity;
import project.pos.MainReturActivity;
import project.pos.R;

public class LogoFragment extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_logo, container, false);
        Animation animZoomIn = AnimationUtils.loadAnimation(getContext(),R.anim.zoom_in);
        view.findViewById(R.id.logo).startAnimation(animZoomIn);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!loginPrefs.isLogin())
                    fragmentManager.beginTransaction()
                        .replace(R.id.frameContainer, new LoginFragment(), Utils.Login_Fragment)
                        .commit();
                else {
//                    if(loginPrefs.getBoolean("isPos"))
                    startActivity(new Intent(getActivity(), MainActivity.class));
//                    else
//                        startActivity(new Intent(getActivity(), MainReturActivity.class));
//                    ((Activity)getContext()).finishAffinity();
                }
            }
        },1000);

        return view;
    }
}
