package fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.android.volley.toolbox.StringRequest;

import java.util.Objects;

import config.Routing;
import helper.Localization;
import preferences.LoginPreferences;
import preferences.UserPreferences;
import request.HttpRequest;

public class BaseFragment extends Fragment {

    public LoginPreferences loginPrefs;
    public UserPreferences userPrefs;
    public Routing routing;
    public Localization localization;
    ProgressDialog progressDialog;
    public FragmentManager fragmentManager;
    StringRequest request;
    HttpRequest httpRequest;
    public Dialog dialog;
    BroadcastReceiver broadcastReceiver;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        routing     = new Routing();
        localization = new Localization();
        loginPrefs  = new LoginPreferences(Objects.requireNonNull(getContext()));
        userPrefs   = new UserPreferences(getContext());
        httpRequest = new HttpRequest(getContext());

        fragmentManager = getFragmentManager();
        progressDialog  = new ProgressDialog(getContext());
        dialog          = new Dialog(getContext());
    }

    public void showProgressDialog(){
        if (!progressDialog.isShowing()){
            progressDialog.show();
        }
    }

    public void hideProgressDialog(){
        if (progressDialog.isShowing()){
            progressDialog.hide();
        }
    }

    public void hideDialog(){
        if (dialog.isShowing())
            dialog.dismiss();
    }
}
