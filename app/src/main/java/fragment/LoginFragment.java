package fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import helper.Alert;
import project.pos.MainActivity;
import project.pos.MainReturActivity;
import project.pos.R;

public class LoginFragment extends BaseFragment implements View.OnClickListener {

    View view;

    EditText etEmail, etPassword;
    String email, pss;
    Boolean showPss = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_login, container, false);
        initView();
        setListener();
        return view;
    }

    private void initView(){
        etEmail     = view.findViewById(R.id.email);
        etPassword  = view.findViewById(R.id.password);
    }

    private void setListener(){
        view.findViewById(R.id.ivShow).setOnClickListener(this);
        view.findViewById(R.id.btnLogin).setOnClickListener(this);

        if (!userPrefs.getEmail().isEmpty()){
            etEmail.setText(userPrefs.getEmail());
            etPassword.requestFocus();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ivShow:
//                new Alert().Success(getContext(), v, "Show Pss");
                if (!showPss){
                    ((ImageView)view.findViewById(R.id.ivShow)).setImageResource(R.drawable.open_eye);
                    showPss = true;
                    etPassword.setInputType(InputType.TYPE_CLASS_TEXT);
                    etPassword.setTransformationMethod(HideReturnsTransformationMethod
                            .getInstance());
                }else{
                    ((ImageView)view.findViewById(R.id.ivShow)).setImageResource(R.drawable.close_eye);
                    showPss = false;
                    etPassword.setInputType(InputType.TYPE_CLASS_TEXT
                            | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    etPassword.setTransformationMethod(PasswordTransformationMethod
                            .getInstance());
                }
                break;
            case R.id.btnLogin:
                validateLogin();
                break;
        }
    }

    private void validateLogin(){
        email   = etEmail.getText().toString();
        pss     = etPassword.getText().toString();

        if (email.isEmpty() || pss.isEmpty()){
            new Alert().Error(getContext(), view, "Data harus diisi semua!");
        }
        else
            login(email, pss);
    }

    public void login(final String email, final String pss){
        request = new StringRequest(Request.Method.POST, routing.login, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Login", response);

                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    boolean success = jsonResponse.getBoolean("success");
                    if (success){
                        JSONObject data = jsonResponse.getJSONObject("data");
                        loginPrefs.setPrefs("id", data.getString("id"));
                        loginPrefs.setPrefs("fullname", data.getString("fullname"));
                        loginPrefs.setPrefs("email", data.getString("email"));
                        loginPrefs.setPrefs("role_id", data.getString("role_id"));
                        loginPrefs.setPrefs("outlet_id", data.getString("outlet_id"));
                        userPrefs.setEmail(loginPrefs.getString("email"));
                        loginPrefs.isLogin(true);
//                        if (((RadioButton)view.findViewById(R.id.pos)).isChecked()) {
//                            loginPrefs.setPrefs("isPos", true);
//                            startActivity(new Intent(getActivity(), MainActivity.class));
//                        }else {
//                            loginPrefs.setPrefs("isPos", false);
//                            startActivity(new Intent(getActivity(), MainReturActivity.class));
//                        }
                        startActivity(new Intent(getActivity(), MainActivity.class));
                        ((Activity)getContext()).finishAffinity();
                    }else{
                        new Alert().Error(getContext(), view, jsonResponse.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                params.put("password", pss);
                return params;
            }
        };

        httpRequest.addQueue(request);
    }


}
