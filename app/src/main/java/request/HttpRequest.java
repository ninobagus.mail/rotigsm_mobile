package request;

/**
 * Created by ASUS on 25/10/2017.
 */

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class HttpRequest {

    public static final String TAG = "HttpRequest";

    RequestQueue requestQueue;

    Context httpRequestContext;

    public HttpRequest(Context context) {
        httpRequestContext = context;
    }

    public <T> void addQueue(Request<T> request) {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(httpRequestContext);
        }
        request.setTag(TAG);
        request.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(request);
    }

    public void cancelQueue(Object tag) {
        if (requestQueue != null) {
            requestQueue.cancelAll(tag);
        }
    }
}
