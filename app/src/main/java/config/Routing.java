package config;

public class Routing {
    public String serviceURL    = "https://rotigsm.com/pos/api/";

    public String login         = serviceURL+"actionLogin";
    public String getProducts   = serviceURL+"getProducts";
    public String getQty        = serviceURL+"getQty";
    public String getRetur      = serviceURL+"getRetur";
    public String getBahan      = serviceURL+"getBahanMentah";
    public String getSupplier   = serviceURL+"getSupplier";
    public String bayar         = serviceURL+"bayarProduk";
    public String addRetur      = serviceURL+"addRetur";
}
