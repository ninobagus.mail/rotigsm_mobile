package config;

public class Utils {
	
	//Email Validation pattern
	public static final String regEx = "\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}\\b";
	public static final String userName = "^[A-Za-z0-9-]*([_A-Za-z0-9])$";

	//Fragments MainLogin
	public static final String Login_Fragment = "Login_Fragment";

	//Fragment Main
	public static final String Home_Fragment  			= "Home_Fragment";
	public static final String Home_Retur_Fragment  	= "Home_Retur_Fragment";
	public static final String Home_Content_Fragment  	= "Home_Content_Fragment";
	public static final String Invoice_Fragment			= "Invoice_Fragment";

	
}
